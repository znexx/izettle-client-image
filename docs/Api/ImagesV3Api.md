# Swagger\Client\ImagesV3Api

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**fetchImage**](ImagesV3Api.md#fetchImage) | **GET** /v3/images/{identifier} | Get an image of required size. If the height or weight is not informed, will return the original size.
[**testRedirectToServerlessImageHandler**](ImagesV3Api.md#testRedirectToServerlessImageHandler) | **GET** /v3/images/test/{identifier} | Test getting an image of required size from AWS Serverless Image Handler solution.


# **fetchImage**
> fetchImage($identifier, $w, $h, $c, $t)

Get an image of required size. If the height or weight is not informed, will return the original size.



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ImagesV3Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$identifier = "identifier_example"; // string | Image identifier
$w = 56; // int | Requested width in pixels
$h = 56; // int | Requested height in pixels
$c = true; // bool | Crop image (true) or fit in box (false). Fit in box is default
$t = "t_example"; // string | Image format, either jpeg, png or gif. Default is jpeg.

try {
    $apiInstance->fetchImage($identifier, $w, $h, $c, $t);
} catch (Exception $e) {
    echo 'Exception when calling ImagesV3Api->fetchImage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **identifier** | **string**| Image identifier |
 **w** | **int**| Requested width in pixels | [optional]
 **h** | **int**| Requested height in pixels | [optional]
 **c** | **bool**| Crop image (true) or fit in box (false). Fit in box is default | [optional]
 **t** | **string**| Image format, either jpeg, png or gif. Default is jpeg. | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: image/jpeg, image/png, image/gif

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **testRedirectToServerlessImageHandler**
> testRedirectToServerlessImageHandler($identifier, $w, $h, $c, $t, $fit)

Test getting an image of required size from AWS Serverless Image Handler solution.



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ImagesV3Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$identifier = "identifier_example"; // string | Image identifier
$w = 56; // int | Requested width in pixels
$h = 56; // int | Requested height in pixels
$c = true; // bool | Crop image (true) or fit in box (false). Fit in box is default
$t = "t_example"; // string | Image format, either jpeg, png or gif. Default is jpeg.
$fit = "fit_example"; // string | Resize mode [cover | contain | fill | inside | outside ]

try {
    $apiInstance->testRedirectToServerlessImageHandler($identifier, $w, $h, $c, $t, $fit);
} catch (Exception $e) {
    echo 'Exception when calling ImagesV3Api->testRedirectToServerlessImageHandler: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **identifier** | **string**| Image identifier |
 **w** | **int**| Requested width in pixels | [optional]
 **h** | **int**| Requested height in pixels | [optional]
 **c** | **bool**| Crop image (true) or fit in box (false). Fit in box is default | [optional]
 **t** | **string**| Image format, either jpeg, png or gif. Default is jpeg. | [optional]
 **fit** | **string**| Resize mode [cover | contain | fill | inside | outside ] | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: image/jpeg, image/png, image/gif

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

