# Swagger\Client\ImagesApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**uploadImageFile**](ImagesApi.md#uploadImageFile) | **POST** /v2/images/organizations/{organizationUuid}/products/upload | Uploads an product image using a file in form data, returns the lookup key for the image and a listof the direct links to the images.
[**uploadProductImage**](ImagesApi.md#uploadProductImage) | **POST** /v2/images/organizations/{organizationUuid}/products | Uploads an product image using image or image url, returns the lookup key for the image and a listof the direct links to the images.
[**uploadProductImageBulk**](ImagesApi.md#uploadProductImageBulk) | **POST** /v2/images/organizations/{organizationUuid}/products/bulk | 
[**uploadProductImageBulkSync**](ImagesApi.md#uploadProductImageBulkSync) | **POST** /v2/images/organizations/{organizationUuid}/products/bulk/sync | 


# **uploadImageFile**
> uploadImageFile($organization_uuid, $file)

Uploads an product image using a file in form data, returns the lookup key for the image and a listof the direct links to the images.

Creates a job for uploading the image, the lookup key is returned directly.The images are eventually available for lookup.Image formats supported are: [BMP,GIF,JPEG,PNG,TIFF].The images must be > 50*50px and < 5MB .

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\ImagesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$file = "/path/to/file.txt"; // \SplFileObject | 

try {
    $apiInstance->uploadImageFile($organization_uuid, $file);
} catch (Exception $e) {
    echo 'Exception when calling ImagesApi->uploadImageFile: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **file** | **\SplFileObject**|  |

### Return type

void (empty response body)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **uploadProductImage**
> \Swagger\Client\Model\ImageUploadResponse uploadProductImage($organization_uuid, $body)

Uploads an product image using image or image url, returns the lookup key for the image and a listof the direct links to the images.

Creates a job for uploading the image, the lookup key is returned directly.The images are eventually available for lookup.Image formats supported are: [BMP,GIF,JPEG,PNG,TIFF].The images must be > 50*50px and < 5MB .

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\ImagesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$body = new \Swagger\Client\Model\ImageUploadRequest(); // \Swagger\Client\Model\ImageUploadRequest | ImageUploadRequest

try {
    $result = $apiInstance->uploadProductImage($organization_uuid, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ImagesApi->uploadProductImage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **body** | [**\Swagger\Client\Model\ImageUploadRequest**](../Model/ImageUploadRequest.md)| ImageUploadRequest | [optional]

### Return type

[**\Swagger\Client\Model\ImageUploadResponse**](../Model/ImageUploadResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **uploadProductImageBulk**
> \Swagger\Client\Model\BulkImageUploadResponse uploadProductImageBulk($organization_uuid, $body)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ImagesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$body = new \Swagger\Client\Model\BulkProductImageUpload(); // \Swagger\Client\Model\BulkProductImageUpload | BulkProductImageUploadRequest

try {
    $result = $apiInstance->uploadProductImageBulk($organization_uuid, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ImagesApi->uploadProductImageBulk: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **body** | [**\Swagger\Client\Model\BulkProductImageUpload**](../Model/BulkProductImageUpload.md)| BulkProductImageUploadRequest | [optional]

### Return type

[**\Swagger\Client\Model\BulkImageUploadResponse**](../Model/BulkImageUploadResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **uploadProductImageBulkSync**
> \Swagger\Client\Model\BulkImageUploadResponse uploadProductImageBulkSync($organization_uuid, $body)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ImagesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$body = new \Swagger\Client\Model\BulkProductImageUpload(); // \Swagger\Client\Model\BulkProductImageUpload | BulkProductImageUploadRequest

try {
    $result = $apiInstance->uploadProductImageBulkSync($organization_uuid, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ImagesApi->uploadProductImageBulkSync: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **body** | [**\Swagger\Client\Model\BulkProductImageUpload**](../Model/BulkProductImageUpload.md)| BulkProductImageUploadRequest | [optional]

### Return type

[**\Swagger\Client\Model\BulkImageUploadResponse**](../Model/BulkImageUploadResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

