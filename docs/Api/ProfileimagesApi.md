# Swagger\Client\ProfileimagesApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteProfileImage**](ProfileimagesApi.md#deleteProfileImage) | **DELETE** /v2/images/profiles/{lookupKey} | Deletes a canonical profile image and all of its existing sizes.


# **deleteProfileImage**
> deleteProfileImage($lookup_key)

Deletes a canonical profile image and all of its existing sizes.

Creates a job for deleting the image.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\ProfileimagesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$lookup_key = "lookup_key_example"; // string | Lookup key that was returned after uploading an image

try {
    $apiInstance->deleteProfileImage($lookup_key);
} catch (Exception $e) {
    echo 'Exception when calling ProfileimagesApi->deleteProfileImage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lookup_key** | **string**| Lookup key that was returned after uploading an image |

### Return type

void (empty response body)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

