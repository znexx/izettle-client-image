# Swagger\Client\ProductimagesApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**fetchProductImage**](ProductimagesApi.md#fetchProductImage) | **GET** /v2/images/product/{identifier} | Get product image of required size


# **fetchProductImage**
> fetchProductImage($identifier, $w, $h, $c, $t)

Get product image of required size



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ProductimagesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$identifier = "identifier_example"; // string | Image identifier
$w = 56; // int | Requested width in pixels
$h = 56; // int | Requested height in pixels
$c = true; // bool | Crop image (true) or fit in box (false). Fit in box is default
$t = "t_example"; // string | Image format, either jpeg, png or gif. Default is jpeg.

try {
    $apiInstance->fetchProductImage($identifier, $w, $h, $c, $t);
} catch (Exception $e) {
    echo 'Exception when calling ProductimagesApi->fetchProductImage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **identifier** | **string**| Image identifier |
 **w** | **int**| Requested width in pixels | [optional]
 **h** | **int**| Requested height in pixels | [optional]
 **c** | **bool**| Crop image (true) or fit in box (false). Fit in box is default | [optional]
 **t** | **string**| Image format, either jpeg, png or gif. Default is jpeg. | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: image/jpeg, image/png, image/gif

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

