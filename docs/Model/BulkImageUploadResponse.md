# BulkImageUploadResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uploaded** | [**\Swagger\Client\Model\SingleBulkImageUploadResponse[]**](SingleBulkImageUploadResponse.md) |  | 
**invalid** | **string[]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


