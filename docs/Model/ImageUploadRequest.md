# ImageUploadRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**image_format** | **string** | Image format of the picture [BMP,GIF,JPEG,PNG,TIFF]. | 
**image_data** | **string[]** | The image as a byte array | [optional] 
**image_url** | **string** | URL to an existing image | [optional] 
**image_lookup_key** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


